#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "monitorso.h"

int availabeResources = 100;
monitor_t monitor;
cond_t condition;

void useResources (int res) {
    while (availabeResources < res) {
        monitor_wait(&monitor,&condition);
    }

    if(availabeResources >= res) {
        enter_monitor(&monitor);
        availabeResources -= res;
        printf("Se folosesc %d resurse | nr. resurse ramase = %d\n",res,availabeResources);
        leave_monitor(&monitor);
    }
}

void freeResources (int res) {
    enter_monitor(&monitor);
    availabeResources += res;
    printf("Se elibereaza %d resurse | nr. resurse ramase = %d\n",res,availabeResources);
    monitor_notify_all(&monitor,&condition);
    leave_monitor(&monitor);
}

void* start (void* r) {
    int res = *(int *) r;


    useResources(res);
    sleep(1);
    freeResources(res);


}

int main() {
    pthread_t pthread[100];

    int n;
    printf("Number of threads to launch = ");
    scanf("%d",&n);

    condition.name = "mayBeSafeToExecute";
    condition.value = 0;

    monitor_init(&monitor,1,&condition);

    for (int i = 0; i < n; i++) {
        int resources = (random()%45)+1;
        if (pthread_create(&pthread[i],NULL,start,&resources)) {
            perror(NULL);
            return errno;
        }
    }

    for (int i = 0; i < n; i++) {
        if(pthread_join(pthread[i],NULL)) {
            perror(NULL);
            return errno;
        }
    }

    return 0;
}
