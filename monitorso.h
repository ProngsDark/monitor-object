#ifndef MONITOR_MONIORSO_H
#define MONITOR_MONIORSO_H

#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>

typedef struct ConditionalArgs
{
    char *name;
    int value;
    int id;
    int threadsInCondition;
} cond_t;

typedef struct Monitor
{
    int isLocked;
    cond_t *conditionalArgs;

    sem_t entrance;
    sem_t *waits;

    pthread_mutex_t access;

    int threadsAtEntrance;

} monitor_t;

int monitor_init(monitor_t *m, int n, cond_t *conds)
{

    /*Memory allocation and initialization of args*/
    m[0].conditionalArgs = malloc(n * sizeof(cond_t));

    for (int i = 0; i < n; ++i)
    {
        m[0].conditionalArgs[i].name = malloc(strlen(conds->name) * sizeof(char));
        strcpy(m[0].conditionalArgs[i].name, conds->name);
        m[0].conditionalArgs[i].value = conds->value;
        m[0].conditionalArgs[i].threadsInCondition = 0;
        m[0].conditionalArgs[i].id = i;
    }

    if (sem_init(&m[0].entrance, 0, 0))
    {
        perror("Unable to initialize monitor");
        return errno;
    }
    else
    {
        printf("Entrance of monitor ready\n");
    }

    /*Semaphores for conditional variables*/
    m[0].waits = malloc(n * sizeof(sem_t));
    for (int i = 0; i < n; ++i)
    {
        if (sem_init(&m[0].waits[i], 0, 0))
        {
            perror("Unable to initialize monitor");
            return errno;
        }
    }

    if (pthread_mutex_init(&m[0].access, NULL))
    {
        perror("Unable to initialize monitor");
        return errno;
    }

    m[0].threadsAtEntrance = 0;

    m[0].isLocked = 0;

    return 0;
}

void monitor_destroy(monitor_t m, int n)
{

    sem_destroy(&m.entrance);
    for (int i = 0; i < n; ++i)
    {
        sem_destroy(&m.waits[i]);
    }
    pthread_mutex_destroy(&m.access);
}

void monitor_lock(monitor_t *m)
{
    if (pthread_mutex_lock(&m[0].access))
    {
        perror(NULL);
        return;
    }
}

void enter_monitor(monitor_t *m)
{
    if (m[0].isLocked)
    {
        m[0].threadsAtEntrance++;
        sem_wait(&m[0].entrance);
    }
    else
    {
        monitor_lock(&m[0]);
    }
}

void schedule(monitor_t *m)
{
    if (m[0].threadsAtEntrance)
    {
        m[0].threadsAtEntrance--;
        sem_post(&m[0].entrance);
    }
    else
    {
        pthread_mutex_unlock(&m[0].access);
    }
}

void monitor_wait(monitor_t *m, cond_t *cond)
{
    cond[0].threadsInCondition++;
    schedule(&m[0]);
    sem_wait(&m[0].waits[cond[0].id]);
    cond[0].threadsInCondition--;
    enter_monitor(&m[0]);
}

void monitor_notify_all(monitor_t *m, cond_t *cond)
{
    for (int i = 0; i < cond[0].threadsInCondition; i++)
    {
        sem_post(&m[0].waits[cond[0].id]);
    }
}

void leave_monitor(monitor_t *m)
{
    schedule(&m[0]);
}

#endif //MONITOR_MONIORSO_H
